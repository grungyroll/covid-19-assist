const logout = () => {
	// remove all key value pairs that is in our local storage
	localStorage.clear()
	// window.location.href = "./index.html"
}

logout()

const Toast = Swal.mixin({
    toast : true,
    position : 'top-end',
    showConfirmButton : false,
    timer : 3000
})

// do not edit
$(function() {
  return $(".modal").on("show.bs.modal", function() {
    var curModal;
    curModal = this;
    $(".modal").each(function() {
      if (this !== curModal) {
        $(this).modal("hide");

      }
    });
  });
});
let travel = 0;
let expo = 0;
let symp = 0;


document.querySelector("#registerButtonNext").addEventListener("click", function(){

	let errors = 0; 
    const title = document.querySelector("#title").value;	
    const nameInput = document.querySelector("#name").value; 	// do not edit
    const name = nameInput.replace(/^\w/, c => c.toUpperCase());
	const age = document.querySelector("#age").value; 
	const residency = document.querySelector("#province").value; 
	  
		// console.log("name" + title + name)
		// console.log("age" + age)
		// console.log("location" + residency)	

        localStorage.setItem("sex", title)
		localStorage.setItem("name", name)
		localStorage.setItem("age", age)
		localStorage.setItem("location", residency)
    if(title==""||nameInput==""||age==""||residency==""||age.includes("e")){
        //alert("Please ensure that all fields are answered")
        $("#registerModal").modal()
        Toast.fire({
            type : 'error',
            title : 'Please check input fields!'
        })  
    } else{
        $("#travelModal").modal()
    }


})

document.querySelector("#emergencyButtonNext").addEventListener("click", function(){

	let errors = 0; 
    const emergency1 = document.querySelector("#emergencySymptoms1").value; 	
	const emergency2 = document.querySelector("#emergencySymptoms2").value; 
	const emergency3 = document.querySelector("#emergencySymptoms3").value; 

	if (document.getElementById('emergencySymptoms1').checked) {
        //console.log(emergency1)
		localStorage.setItem("emergencySymptoms1", emergency1)  
        window.location.href="./assessment.html"

    }

    if (document.getElementById('emergencySymptoms2').checked) {
        //console.log(emergency2)
        localStorage.setItem("emergencySymptoms2", emergency2)
        window.location.href="./assessment.html"
    }

	if (document.getElementById('emergencySymptoms3').checked) {
    	 //console.log(emergency3)
		localStorage.setItem("emergencySymptoms3", emergency3)
    }
})


document.querySelector("#travelButtonNext").addEventListener("click", function(){

	let errors = 0; 
    const travelYes = document.querySelector("#travelYes").value; 	
	const travelNo = document.querySelector("#travelNo").value; 
	 
    if (document.getElementById('travelYes').checked) {
        //console.log("With travel history in the past 14 days.")
		localStorage.setItem("travelHistory", 1)
        $("#symptomsModal").modal()
        travel = 1;
        
    } 
    else if (document.getElementById('travelNo').checked) {
        //console.log("Without travel history in the past 14 days.")
		localStorage.setItem("travelHistory", 0)
        $("#exposureModal").modal();
        travel = 0;
	}
    else{
        Toast.fire({
            type : 'error',
            title : 'No input.'
        })  
        $("#travelModal").modal();

    }

})


document.querySelector("#exposureButtonNext").addEventListener("click", function(){

    const exposure1 = document.querySelector("#exposure1").value; 	
	const exposure2 = document.querySelector("#exposure2").value; 
	const exposure3 = document.querySelector("#exposure3").value; 
	const exposure4 = document.querySelector("#exposure4").value; 
	const exposure5 = document.querySelector("#exposure5").value; 

	if (document.getElementById('exposure1').checked) {
	   	//console.log(exposure1)
		localStorage.setItem("expo1", exposure1)
        $("#symptomsModal").modal()
        expo = 1;

    } if (document.getElementById('exposure2').checked) {
        //console.log(exposure2)
		localStorage.setItem("expo2", exposure2)
        $("#symptomsModal").modal()
        expo = 1;
    }

    if (document.getElementById('exposure3').checked) {
        //console.log(exposure3)
		localStorage.setItem("expo3", exposure3)
        $("#symptomsModal").modal()
        expo = 1;
    }
    
    if (document.getElementById('exposure4').checked) {
        //console.log(exposure4)
		localStorage.setItem("expo4", exposure4)
        $("#symptomsModal").modal()
        expo = 1;
	} 

    if (document.getElementById('exposure5').checked) {
        //console.log(exposure5)
		localStorage.setItem("expo5", exposure5)
        //console.log("PUM")
        expo = 0;


    } 
    if(expo==1 || document.getElementById('exposure5').checked){
        $("#symptomsModal").modal()
    }
    else{
        $("#exposureModal").modal()
        Toast.fire({
            type : 'error',
            title : 'No input.'
        })  
    }


})



//------------
document.querySelector("#exposure5").addEventListener("click", function(){
    if (document.getElementById("exposure5").checked == true){
        for(let y=1;y<=4;y++){
            document.getElementById("exposure"+y).checked = false;
        }
    }
})

    for(i=1;i<=4;i++){
        document.querySelector("#exposure"+i).addEventListener("click", function(){
        document.getElementById("exposure5").checked = false;
    })
    }
     for(i=1;i<=10;i++){
        document.querySelector("#symptoms"+i).addEventListener("click", function(){
        document.getElementById("symptoms11").checked = false;
    })
    }   

document.querySelector("#symptoms11").addEventListener("click",function(){
    if(document.getElementById("symptoms11").checked==true){
        for(x=1;x<=10;x++){
            document.getElementById("symptoms"+x).checked = false;
        }
    }
})





document.querySelector("#symptomsButtonNext").addEventListener("click", function(){

   const symptoms1 = document.querySelector("#symptoms1").value; 	
	const symptoms2 = document.querySelector("#symptoms2").value; 
	const symptoms3 = document.querySelector("#symptoms3").value; 
	const symptoms4 = document.querySelector("#symptoms4").value; 
	const symptoms5 = document.querySelector("#symptoms5").value; 
	const symptoms6 = document.querySelector("#symptoms6").value; 
	const symptoms7 = document.querySelector("#symptoms7").value; 
	const symptoms8 = document.querySelector("#symptoms8").value; 
	const symptoms9 = document.querySelector("#symptoms9").value; 
	const symptoms10 = document.querySelector("#symptoms10").value; 
	const symptoms11 = document.querySelector("#symptoms11").value; 
	  

	if (document.getElementById('symptoms1').checked) {
	   	//console.log(symptoms1)
		localStorage.setItem("symptoms1", symptoms1)
        window.location.href="./assessment.html"
        symp = 1;
    }

    if (document.getElementById('symptoms2').checked) {
        //console.log(symptoms2)
		localStorage.setItem("symptoms2", symptoms2)
        window.location.href="./assessment.html"
        symp = 1;
    }

    if (document.getElementById('symptoms3').checked) {
        //console.log(symptoms3)
		localStorage.setItem("symptoms3", symptoms3)
         window.location.href="./assessment.html"
         symp = 1;
    }
    
    if (document.getElementById('symptoms4').checked) {
        //console.log(symptoms4)
		localStorage.setItem("symptoms4", symptoms4)
         window.location.href="./assessment.html"
         symp = 1;
	} 

    if (document.getElementById('symptoms5').checked) {
        //console.log(symptoms5)
		localStorage.setItem("symptoms5", symptoms5)
         window.location.href="./assessment.html"
         symp = 1;
    }

    if (document.getElementById('symptoms6').checked) {
	   	//console.log(symptoms6)
		localStorage.setItem("symptoms6", symptoms6)
         window.location.href="./assessment.html"
         symp = 1;
    }

    if (document.getElementById('symptoms7').checked) {
        //console.log(symptoms7)
		localStorage.setItem("symptoms7", symptoms7)
         window.location.href="./assessment.html"
         symp = 1;
    }

    if (document.getElementById('symptoms8').checked) {
        //console.log(symptoms8)
		localStorage.setItem("symptoms8", symptoms8)
         window.location.href="./assessment.html"
         symp = 1;
    }
    
    if (document.getElementById('symptoms9').checked) {
        //console.log(symptoms9)
		localStorage.setItem("symptoms9", symptoms9)
         window.location.href="./assessment.html"
         symp = 1;
	} 

    if (document.getElementById('symptoms10').checked) {
        //console.log(symptoms10)
		localStorage.setItem("symptoms10", symptoms10)
         window.location.href="./assessment.html"
         symp = 1;
    }

    if (document.getElementById('symptoms11').checked) {
        //console.log(symptoms11)
		localStorage.setItem("symptoms11", symptoms11)
         window.location.href="./assessment.html"
         symp =0;
    }
    if(symp==1||document.getElementById('symptoms11').checked){
        let assessmentnum = travel+expo+symp;
    localStorage.setItem("assessmentnum", assessmentnum)
         window.location.href="./assessment.html"
    } else{
        $("#symptomsModal").modal()
        Toast.fire({
            type : 'error',
            title : 'No input.'
        })
    }

  

    

})
		



// document.querySelector("#severeButtonNext").addEventListener("click", function(){

//     const severe1 = document.querySelector("#severeSymptoms1").value; 	
// 	const severe2 = document.querySelector("#severeSymptoms2").value; 
// 	const severe3 = document.querySelector("#severeSymptoms3").value; 
	
// 	if (document.getElementById('severeSymptoms1').checked) {
//         console.log(severe1)
// 		localStorage.setItem("severe1", severe1)
// 	} 

//     if (document.getElementById('severeSymptoms2').checked) {
//         console.log(severe2)
// 		localStorage.setItem("severe2", severe2)
//     }

//     if (document.getElementById('severeSymptoms3').checked) {
//         console.log(severe3)
// 		localStorage.setItem("severe3", severe3)
//     }

//     else  {
// 	}

// })